# Internet Freedom Foundation

---

## Changing content (non-technical)

If you wish to suggest a change to the Internet Freedom Foundation website, just
click on `posts/` or `pages/`, find the content you wish to change, hit `Edit`
and submit your changes as a 'Merge Request'.

Type a short description of what you changed and why, and we will review it. We
may have follow-up questions or suggestions for you, please check your email!

### Adding images or files

You can upload files into `uploads/` and insert them using MarkDown.

## Editing the template

- Download [hugo](https://gohugo.io)
- Fork this repository and clone your fork
- Run `hugo server` in your cloned directory
- Open http://localhost:1313/internetfreedom.in
- Edit files, refresh your browser, rinse, repeat
- When ready, commit, push and open a merge request

